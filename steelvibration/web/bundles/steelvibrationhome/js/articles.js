
//click on sumit a comment : send comment in ajax and get the answer from controller
$('.btn.btn-primary.btn-submit-comment').click(function() {
	var id = $.parseJSON($(this).attr('data-button'));
	var comment = $("#CommentFormModal"+id).find("textarea").val();
	//ajax request
	var ajaxpath = getAjaxControllerPath();
	$.post(ajaxpath,
		{requestType : 'SUBMIT_ARTICLE_COMMENT',articleId : id, comment: comment},
		function(response)
		{
			var currentNumberArticle = $("#panel-article-"+id).find(".badge").html();
			myInteger = parseInt(currentNumberArticle);
			//display ajax results
			$("#panel-article-"+id).find(".badge").html(myInteger + 1);
		}
	);
	
});

//click on displayComment : get the article comment.
$('.btn.btn-default.displayComment').click(function()
{
	var id = $.parseJSON($(this).attr('data-button'));
	//ajax request
	var ajaxpath = getAjaxControllerPath();
	$.post(ajaxpath,
		{requestType : 'GET_ARTICLE_COMMENTS', articleId : id},
		function(response)
		{
			//display ajax results
			$('#CommentModal'+id).find('.modal-body').html(response);
		}
	);
});

function getAjaxControllerPath(){
	return $("#ajax-path").attr('data-ajaxController');
}