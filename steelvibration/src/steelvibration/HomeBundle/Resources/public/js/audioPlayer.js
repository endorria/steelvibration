var playerCompatible = true // compatibilité HTML5 / MediaQueries / Progress / Audio...
var play = false; // default: true - autoplay
// liste des pistes a jouer
var currentTrack = 1;
var timer = null; // objet interval
var totalTrack = 0;
	
function audioPlayerJS(){	
		// feature detection
		if(!Modernizr.mq('only all')){
			playerCompatible = false;
			$('#cssSupport').css('color','#FF0000');
		}
		
		if(!Modernizr.audio.mp3){
			playerCompatible = false;
			$('#audioSupport').css('color','#FF0000');
		}
		
		if(!Modernizr.inputtypes.range){
			playerCompatible = false;
			$('#rangeSupport').css('color','#FF0000');
		}
		// déblocage du contenu
		if(playerCompatible){
			$('#compatibility').css('display','none');
			$('header').css('display','block');
			$('#contentContainer').css('display','block');
		}else{
			$('#compatibility').css('display','block');
			$('header').css('display','none');
			$('#contentContainer').css('display','none');
		}
		
		// localstorage optionnel
		if(Modernizr.localstorage){
			var savedVol = localStorage.getItem('volume');
			if(savedVol != null){
				$('#volume').val(savedVol);
				$('#audioPlayer')[0].volume = savedVol / 100;
			}
		}
		
		totalTrack = $("#audioPlayList li").length;
		changeSong(1);
		displayDurationAndTotalTime();
		// controle du lecteur
		$('#btnReverse').click(function(){
			currentTrack--;
			if(currentTrack == 0){
				currentTrack = totalTrack;
			}
			changeSong(currentTrack);
			if(play){
				$('#audioPlayer')[0].play();
			}
		});
			
		$('#btnPlayPause').click(function(){
			$('#playItem').toggleClass('glyphicon-pause');
			if(play){
				play = false;
				$('#audioPlayer')[0].pause();
				clearInterval(timer);
			}else{
				play = true;
				$('#audioPlayer')[0].play();
				displayDurationAndTotalTime();
				timer = setInterval("intervalRefresh()",1000);
			}
		});
	
		$('#btnForward').click(function(){
			currentTrack++;
			if(currentTrack > totalTrack){
				currentTrack = 1;
			}
			changeSong(currentTrack);
			if(play){
				$('#audioPlayer')[0].play();
			}
		});
		
		$('#volume').change(function(ev){
			ev.preventDefault();
			var vol = document.getElementById('volume').value;
			$('#audioPlayer')[0].volume = vol / 100;
			if(Modernizr.localstorage){	localStorage.setItem('volume', vol); }
		});

		$("#audioPlayer").bind("ended", function(){
	    	currentTrack++;
	    	if(currentTrack > totalTrack){
				currentTrack = 1;
			}
			changeSong(currentTrack);
			if(play){
				$('#audioPlayer')[0].play();
			}
	      });
	}
	
function intervalRefresh(){
	displayDurationAndTotalTime();
	ajustProgression();
}

function displayDurationAndTotalTime(){
	$('#currentTime').html(convertSecToMinutes($('#audioPlayer')[0].currentTime));
	$('#totalTime').html(convertSecToMinutes($('#audioPlayer')[0].duration));
}

function ajustProgression(){
	var player = $('#audioPlayer')[0];
	var current = player.currentTime;
	var total = player.duration;
	
	var progress = (current * 100) / total;
	
	$('#avancement').val(Math.round(progress));
}

function changeSong(id){
	var newSong = $('a[data-track="'+id+'"]').attr('data-song');
	var newSongTitle = $('a[data-track="'+id+'"]').attr('data-title');
	$("#audioPlayer").attr('src', newSong);
    $("#audioPlayer").attr('data-current', id);
    $("#playedMusicTitle").text("-"+newSongTitle+"-");
}

function convertSecToMinutes(time){
var ret = null;
if(time != 0){
	var secondes = time % 60;
	var entier = (time - secondes) / 60;
	ret = entier.toString();
		
	if(secondes < 10){
		ret += ":" + "0" + Math.round(secondes).toString();
	}else{
		ret += ":" + Math.round(secondes).toString();
	}
	
}else{
	ret = "0:00";
}

if(ret == "NaN:NaN"){
	ret = "0:00";
}

	return ret
}
			