// //offset the origin on the z-axis to make the spins more interesting.
// TweenLite.set($(".logoWelcome"), {transformOrigin:"center center -150px"});
// //pulsate the box using scaleX and scaleY
// TweenMax.to($(".logoWelcome"), 1.2, {scaleX:0.8, scaleY:0.8, force3D:true, yoyo:true, repeat:-1, ease:Power1.easeInOut});
(function() {

  $(function() {
    return console.log("fired j query");
  });

  window.requestAnimationFrame || (window.requestAnimationFrame = window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback, element) {
    return window.setTimeout(function() {
      return callback(+new Date());
    }, 1000 / 60);
  });
  FirstPageSite = (function() {

    FirstPageSite.prototype.loader = null;


    function FirstPageSite() {
      console.log("first page site constructor");
      TweenLite.to($("#loaderIcon"), 2, {rotation:360, transformOrigin:"center center"});
      this.loadImage();
      this.windowWith = $(window).width();
      $(window).resize(this.resize);
    }

    FirstPageSite.prototype.completePreloadingImages = function() {
      console.log("FirstPage::completePreloadingImages");
      this.elementsLoaded = true;
      this.transitionInPresentation();
    };

    FirstPageSite.prototype.loadImage = function() {
      var _this = this;
      console.log("loading images");
      this.loader = new createjs.LoadQueue();
      this.loader.loadManifest("../albums/photos/accordage.json");
      // this.loader.loadFile({
      //   id: "album1",
      //   src: "../images/mosaic/album1/*"
      // });
      return this.loader.addEventListener("complete", function(e) {
        return _this.completePreloadingImages(e);
      });
    };

    FirstPageSite.prototype.transitionInPresentation = function() {
      console.log("transition in");
      TweenMax.to($(".myPreloader"), 2, {
        css: {
          transform: "translateY(-100%) translateZ(0)"
        },
        delay: 0,
        ease: Expo.easeInOut,
        onComplete: this.hideLayerPreentation
      });
    };
  
    $(".siteButton").click(function() {
        console.log("siteButton clicked");
        
      TweenMax.to($(".welcomePage"), 2, {
        css: {
          transform: "translateY(-100%) translateZ(0)"
        },
        delay: 0,
        ease: Expo.easeInOut
      });

     setTimeout(function(){window.location = $(".siteButton").attr('value');}, 600);


    });
    FirstPageSite.prototype.hideLayerPresentation = function() {
      $(".myPreloader").css("display", "none");
      $(".siteButton").mouseover(function() {
        return TweenMax.to($(".siteButton"), 1, {
          css: {
            transform: "translateY(-66%) translateZ(0)"
          },
          delay: 0,
          ease: Expo.easeOut
        });
      });
      $(".siteButton").mouseout(function() {
        return TweenMax.to($(".siteButton"), 1, {
          css: {
            transform: "translateY(0%) translateZ(0)"
          },
          delay: 0,
          ease: Expo.easeOut
        });
      });
      
    };

    return FirstPageSite;

  })();

 firstPageSite = new FirstPageSite();

}).call(this);