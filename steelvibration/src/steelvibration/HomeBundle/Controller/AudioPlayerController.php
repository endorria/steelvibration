<?php

namespace steelvibration\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AudioPlayerController extends Controller
{

    public function defaultAction($max = 5)
    {
    	$playlist = $this -> getDoctrine()
    				->getRepository('steelvibrationHomeBundle:Music')
    				->findByisinplaylist(true);

    	if (!$playlist) {
	        throw $this->createNotFoundException(
	            'Aucune musique trouvée dans la playlist : '
	        );
		}

        return $this->render('steelvibrationHomeBundle::audioPlayer.html.twig', array('playlist' => $playlist));
    }
}
?>