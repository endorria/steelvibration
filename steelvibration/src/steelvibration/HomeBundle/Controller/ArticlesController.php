<?php
namespace steelvibration\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticlesController extends Controller
{

    public function defaultAction()
    {
    	$articles = $this -> getDoctrine()
    				->getRepository('steelvibrationHomeBundle:Article')
    				->findAll();

    	if (!$articles) {
	        throw $this->createNotFoundException(
	            'Aucun article trouvé dans la base '
	        );
		}

        return $this->render('steelvibrationHomeBundle::articles.html.twig', array('articles' => $articles));
    }

    public function displayArticleAction($article)
    {
 
    	return $this->render('steelvibrationHomeBundle::article.html.twig',array('article' => $article));
    }
}
?>