<?php
namespace steelvibration\HomeBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use steelvibration\HomeBundle\Entity;

use Doctrine\Common\Collections\Criteria;

class AjaxQueryController extends Controller
{
	public function defaultAction()
	{
		$request = $this->getRequest();
		$requestType = $request->request->get('requestType');
		$response = 'request unknown';
		switch ($requestType) {
		    case "SUBMIT_ARTICLE_COMMENT":
		    	$articleId = $request->request->get('articleId');
		    	$comment = $request->request->get('comment');
		        return self::submitArticleComment($articleId, $comment);
		        break;
		    case "GET_ARTICLE_COMMENTS":
		    	$articleId = $request->request->get('articleId');
		        return self::getArticleComments($articleId);
		        break;
		}

		return new response($response);
	}

	private function submitArticleComment($articleId, $commentContent)
	{
		$response = "id: ".$articleId." ,comment: ".$commentContent;

		/*$article = $this -> getDoctrine()
    				->getRepository('steelvibrationHomeBundle:Article')
    				->find($articleId);

		$comment = new Comment();
		$comment.setContentcomment($commentContent);
		$comment.setArticlearticle($articles);

		$em = $this->getDoctrine()->getManager();
    	$em->persist($comment);
    	$em->flush();*/


		return new response($response);
	}

	public function getArticleComments($articleId)
	{
		$comments = $this -> getDoctrine()
    				->getRepository('steelvibrationHomeBundle:Comment')
    				->findAll();    				

		return $this->render('steelvibrationHomeBundle::articleComments.html.twig',array('comments' => $comments, 'articleId' => $articleId));
	}
}

?>