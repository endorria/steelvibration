<?php

namespace steelvibration\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class MediaController extends Controller
{

    public function getAlbumsAction($max = 5, $type)
    {
    	$albums = $this -> getDoctrine()
    				->getRepository('steelvibrationHomeBundle:Mediaalbum')
    				->findBytypemediaalbum($type);

    	if (!$albums) {
	        throw $this->createNotFoundException(
	            'Aucun album photo trouvé '
	        );
		}

        return $this->render('steelvibrationHomeBundle::mosaicAlbum.html.twig', array('albums' => $albums));
    }

    public function getMediaFromAlbumAction()
    {
        
        $request = $this->getRequest();    

        if($request->isXmlHttpRequest())
        {
            $idAlbum = $request->get('idAlbum');

            $em = $this->getDoctrine()->getEntityManager();
            $query = $em->createQuery('SELECT m FROM steelvibration\HomeBundle\Entity\Media m WHERE m.mediaalbummediaalbum=:id');
            $query->setParameter('id', $idAlbum);
            $result = $query->getArrayResult();

            
            //return new Response(json_encode($result), 200);
           //return $this->render('steelvibrationHomeBundle::mosaicPhoto.html.twig', array('medias' => $medias));
            $response = new Response();
            $medias = json_encode($result);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($medias);
            return $response;
        }
        return new Response('Erreur');
    }

    public function showMediasAction() {
        $request = $this->getRequest();    
        $medias =  $request->get('result');

         return $this->render('steelvibrationHomeBundle::mosaicPhoto.html.twig', array('medias' => $medias));
    }
}
?>