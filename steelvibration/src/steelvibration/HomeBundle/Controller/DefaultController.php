<?php

namespace steelvibration\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('steelvibrationHomeBundle::firstPage.html.twig');
    }
    public function homeAction()
    {
        return $this->render('steelvibrationHomeBundle::index.html.twig');
    }
    public function translationAction($name)
    {
    	return $this->render('steelvibrationHomeBundle::translation.html.twig', array(
    		'name' => $name
    		));
    }
}
