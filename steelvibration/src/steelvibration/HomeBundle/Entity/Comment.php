<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comment", indexes={@ORM\Index(name="IDX_9474526CE615074A", columns={"Article_idArticle"})})
 * @ORM\Entity
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idComment", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idcomment;

    /**
     * @var string
     *
     * @ORM\Column(name="contentComment", type="string", length=300, nullable=true)
     */
    private $contentcomment;

    /**
     * @var string
     *
     * @ORM\Column(name="authorComment", type="string", length=45, nullable=true)
     */
    private $authorcomment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="depositeDateComment", type="datetime", nullable=true)
     */
    private $depositedatecomment;

    /**
     * @var \Article
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Article")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Article_idArticle", referencedColumnName="idArticle")
     * })
     */
    private $articlearticle;



    /**
     * Set idcomment
     *
     * @param integer $idcomment
     * @return Comment
     */
    public function setIdcomment($idcomment)
    {
        $this->idcomment = $idcomment;
    
        return $this;
    }

    /**
     * Get idcomment
     *
     * @return integer 
     */
    public function getIdcomment()
    {
        return $this->idcomment;
    }

    /**
     * Set contentcomment
     *
     * @param string $contentcomment
     * @return Comment
     */
    public function setContentcomment($contentcomment)
    {
        $this->contentcomment = $contentcomment;
    
        return $this;
    }

    /**
     * Get contentcomment
     *
     * @return string 
     */
    public function getContentcomment()
    {
        return $this->contentcomment;
    }

    /**
     * Set authorcomment
     *
     * @param string $authorcomment
     * @return Comment
     */
    public function setAuthorcomment($authorcomment)
    {
        $this->authorcomment = $authorcomment;
    
        return $this;
    }

    /**
     * Get authorcomment
     *
     * @return string 
     */
    public function getAuthorcomment()
    {
        return $this->authorcomment;
    }

    /**
     * Set depositedatecomment
     *
     * @param \DateTime $depositedatecomment
     * @return Comment
     */
    public function setDepositedatecomment($depositedatecomment)
    {
        $this->depositedatecomment = $depositedatecomment;
    
        return $this;
    }

    /**
     * Get depositedatecomment
     *
     * @return \DateTime 
     */
    public function getDepositedatecomment()
    {
        return $this->depositedatecomment;
    }

    /**
     * Set articlearticle
     *
     * @param \steelvibration\HomeBundle\Entity\Article $articlearticle
     * @return Comment
     */
    public function setArticlearticle(\steelvibration\HomeBundle\Entity\Article $articlearticle)
    {
        $this->articlearticle = $articlearticle;
    
        return $this;
    }

    /**
     * Get articlearticle
     *
     * @return \steelvibration\HomeBundle\Entity\Article 
     */
    public function getArticlearticle()
    {
        return $this->articlearticle;
    }
}
