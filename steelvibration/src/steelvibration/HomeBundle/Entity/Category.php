<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idCategory", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcategory;

    /**
     * @var string
     *
     * @ORM\Column(name="nameCategory", type="string", length=45, nullable=false)
     */
    private $namecategory;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionCategory", type="string", length=100, nullable=true)
     */
    private $descriptioncategory;



    /**
     * Get idcategory
     *
     * @return integer 
     */
    public function getIdcategory()
    {
        return $this->idcategory;
    }

    /**
     * Set namecategory
     *
     * @param string $namecategory
     * @return Category
     */
    public function setNamecategory($namecategory)
    {
        $this->namecategory = $namecategory;
    
        return $this;
    }

    /**
     * Get namecategory
     *
     * @return string 
     */
    public function getNamecategory()
    {
        return $this->namecategory;
    }

    /**
     * Set descriptioncategory
     *
     * @param string $descriptioncategory
     * @return Category
     */
    public function setDescriptioncategory($descriptioncategory)
    {
        $this->descriptioncategory = $descriptioncategory;
    
        return $this;
    }

    /**
     * Get descriptioncategory
     *
     * @return string 
     */
    public function getDescriptioncategory()
    {
        return $this->descriptioncategory;
    }
}
