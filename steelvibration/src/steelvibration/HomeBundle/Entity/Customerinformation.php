<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customerinformation
 */
class Customerinformation
{
    /**
     * @var integer
     */
    private $idcustomerinformation;

    /**
     * @var string
     */
    private $billingaddress;

    /**
     * @var string
     */
    private $deliveryaddress;

    /**
     * @var string
     */
    private $namecustomer;

    /**
     * @var string
     */
    private $prenomcustomer;

    /**
     * @var string
     */
    private $phonecustomer;

    /**
     * @var string
     */
    private $emailcustomer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idorderalbum;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idorderalbum = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get idcustomerinformation
     *
     * @return integer 
     */
    public function getIdcustomerinformation()
    {
        return $this->idcustomerinformation;
    }

    /**
     * Set billingaddress
     *
     * @param string $billingaddress
     * @return Customerinformation
     */
    public function setBillingaddress($billingaddress)
    {
        $this->billingaddress = $billingaddress;
    
        return $this;
    }

    /**
     * Get billingaddress
     *
     * @return string 
     */
    public function getBillingaddress()
    {
        return $this->billingaddress;
    }

    /**
     * Set deliveryaddress
     *
     * @param string $deliveryaddress
     * @return Customerinformation
     */
    public function setDeliveryaddress($deliveryaddress)
    {
        $this->deliveryaddress = $deliveryaddress;
    
        return $this;
    }

    /**
     * Get deliveryaddress
     *
     * @return string 
     */
    public function getDeliveryaddress()
    {
        return $this->deliveryaddress;
    }

    /**
     * Set namecustomer
     *
     * @param string $namecustomer
     * @return Customerinformation
     */
    public function setNamecustomer($namecustomer)
    {
        $this->namecustomer = $namecustomer;
    
        return $this;
    }

    /**
     * Get namecustomer
     *
     * @return string 
     */
    public function getNamecustomer()
    {
        return $this->namecustomer;
    }

    /**
     * Set prenomcustomer
     *
     * @param string $prenomcustomer
     * @return Customerinformation
     */
    public function setPrenomcustomer($prenomcustomer)
    {
        $this->prenomcustomer = $prenomcustomer;
    
        return $this;
    }

    /**
     * Get prenomcustomer
     *
     * @return string 
     */
    public function getPrenomcustomer()
    {
        return $this->prenomcustomer;
    }

    /**
     * Set phonecustomer
     *
     * @param string $phonecustomer
     * @return Customerinformation
     */
    public function setPhonecustomer($phonecustomer)
    {
        $this->phonecustomer = $phonecustomer;
    
        return $this;
    }

    /**
     * Get phonecustomer
     *
     * @return string 
     */
    public function getPhonecustomer()
    {
        return $this->phonecustomer;
    }

    /**
     * Set emailcustomer
     *
     * @param string $emailcustomer
     * @return Customerinformation
     */
    public function setEmailcustomer($emailcustomer)
    {
        $this->emailcustomer = $emailcustomer;
    
        return $this;
    }

    /**
     * Get emailcustomer
     *
     * @return string 
     */
    public function getEmailcustomer()
    {
        return $this->emailcustomer;
    }

    /**
     * Add idorderalbum
     *
     * @param \steelvibration\HomeBundle\Entity\Album $idorderalbum
     * @return Customerinformation
     */
    public function addIdorderalbum(\steelvibration\HomeBundle\Entity\Album $idorderalbum)
    {
        $this->idorderalbum[] = $idorderalbum;
    
        return $this;
    }

    /**
     * Remove idorderalbum
     *
     * @param \steelvibration\HomeBundle\Entity\Album $idorderalbum
     */
    public function removeIdorderalbum(\steelvibration\HomeBundle\Entity\Album $idorderalbum)
    {
        $this->idorderalbum->removeElement($idorderalbum);
    }

    /**
     * Get idorderalbum
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdorderalbum()
    {
        return $this->idorderalbum;
    }
}
