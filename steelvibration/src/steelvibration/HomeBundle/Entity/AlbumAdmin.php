<?php
namespace steelvibration\HomeBundle\Entity;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AlbumAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('Form')
            ->add('titleAlbum', 'text', array('label' => 'Title'))
            ->add('priceAlbum', 'text', array('label' => 'Price '))
            ->add('imagePath', 'text', array('label' => 'Image ')) 
        ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
       //     ->add('title')
        //    ->add('author')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titleAlbum')
            ->add('priceAlbum')
            ->add('imagePath')
            ->add('_action','actions', array('actions' => array('delete' => array(),
                                                                'edit' => array()
                                                                )
                                            )
                  )
            ;
        ;
    }
}