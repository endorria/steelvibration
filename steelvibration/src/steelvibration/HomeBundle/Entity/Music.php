<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Music
 */
class Music
{
    /**
     * @var integer
     */
    private $idmusic;

    /**
     * @var string
     */
    private $titlemusic;

    /**
     * @var string
     */
    private $pathmusic;

    /**
     * @var boolean
     */
    private $isinplaylist;

    /**
     * @var \steelvibration\HomeBundle\Entity\Album
     */
    private $albumalbum;


    /**
     * Get idmusic
     *
     * @return integer 
     */
    public function getIdmusic()
    {
        return $this->idmusic;
    }

    /**
     * Set titlemusic
     *
     * @param string $titlemusic
     * @return Music
     */
    public function setTitlemusic($titlemusic)
    {
        $this->titlemusic = $titlemusic;
    
        return $this;
    }

    /**
     * Get titlemusic
     *
     * @return string 
     */
    public function getTitlemusic()
    {
        return $this->titlemusic;
    }

    /**
     * Set pathmusic
     *
     * @param string $pathmusic
     * @return Music
     */
    public function setPathmusic($pathmusic)
    {
        $this->pathmusic = $pathmusic;
    
        return $this;
    }

    /**
     * Get pathmusic
     *
     * @return string 
     */
    public function getPathmusic()
    {
        return $this->pathmusic;
    }

    /**
     * Set isinplaylist
     *
     * @param boolean $isinplaylist
     * @return Music
     */
    public function setIsinplaylist($isinplaylist)
    {
        $this->isinplaylist = $isinplaylist;
    
        return $this;
    }

    /**
     * Get isinplaylist
     *
     * @return boolean 
     */
    public function getIsinplaylist()
    {
        return $this->isinplaylist;
    }

    /**
     * Set albumalbum
     *
     * @param \steelvibration\HomeBundle\Entity\Album $albumalbum
     * @return Music
     */
    public function setAlbumalbum(\steelvibration\HomeBundle\Entity\Album $albumalbum = null)
    {
        $this->albumalbum = $albumalbum;
    
        return $this;
    }

    /**
     * Get albumalbum
     *
     * @return \steelvibration\HomeBundle\Entity\Album 
     */
    public function getAlbumalbum()
    {
        return $this->albumalbum;
    }
}
