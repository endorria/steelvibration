<?php
namespace steelvibration\HomeBundle\Entity;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ArticleAdmin extends Admin
{
  

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titleArticle')
            ->add('contentArticle','textarea', array('attr' => array('cols' => '5', 'rows' => '45')))
            ->add('authorArticle')
            ->add('pathImage')
            ->add('category')
            ->add('_action', 'actions', array( 'actions' => array('delete' => array(),
                                                                  'edit' => array(),
                                                                  )
                                              )
                  )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            /*->add('titleArticle')
            ->add('contentArticle')
            ->add('authorArticle')
            ->add('category')
            ->add('pathImage')*/
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
                ->add('titleArticle')
                ->add('contentArticle')
                ->add('authorArticle')
                ->add('category')
                ->add('pathImage')
            ->end()
            ->with('Management')

            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
            ->with('Management', array('class' => 'col-md-6'))
        ;

        $formMapper
            ->with('General')
                ->add('titleArticle', 'text', array('label' => 'Title'))
                ->add('category','text', array('label' => 'Category'))
                ->add('contentArticle','textarea', array('label' => 'Body article'))
                ->add('authorArticle', 'text', array('label' => 'Author'))
                ->add('pathImage','file', array('required' => false))
            ->end()
            ->with('Management')
                ->add('depositeDateArticle', 'date', array('label'=> 'Deposited at','required' => false))
                ->add('targetedDateArticle', 'date', array('label'=> 'Target at','required' => false))
                ->add('modifiedDateArticle', 'date', array('label'=> 'Modify at','required' => false))
            ->end()
        ;

        }
}
