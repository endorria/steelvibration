<?php
namespace steelvibration\HomeBundle\Entity;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CustomerinformationAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('prenomCustomer','text',   array('label' => 'Customer First Name'))
            ->add('nameCustomer','text',     array('label' => 'Customer Last Name' ))
            ->add('billingAddress', 'text',  array('label' => 'Billing Address', 'help'=> 'The address where you do the order!'))
            ->add('deliveryAddress', 'text', array('label' => 'Delivery Address','help'=> 'The address where you want the order to be delivered!'))
            ->add('phoneCustomer','text',    array('label' => 'Customer Phone' ))
            ->add('emailCustomer','email',    array('label' => 'Customer Email' ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
       //    ->add('prenomCustomer')
        //    ->add('nameCustomer')
         //   ->add('billingAddress')
          //  ->add('deliveryAddress')
            //->add('phoneCustomer')
            //->add('emailCustomer')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('prenomCustomer')
            ->add('nameCustomer')
            ->add('billingAddress')
            ->add('deliveryAddress')
            ->add('phoneCustomer')
            ->add('emailCustomer')
            ->add('_action','actions', array('actions' => array('delete' => array(),
                                                                'edit' => array()
                                                                )
                                            )
                  )
        ;
    }
}