<?php
namespace steelvibration\HomeBundle\Entity;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CategoryAdmin extends Admin
{

    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('nameCategory')
            ->add('descriptionCategory')
        ;
    }


    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Form')
                ->add('nameCategory','text', array('label' => 'Category'))
                ->add('descriptionCategory' ,'textarea', array('label' => 'Description', 'required' => false))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        // ->add('nameCategory')
       ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nameCategory')
            ->add('descriptionCategory')
             ->add('_action','actions', array('actions' => array('delete' => array(),
                                                                'edit' => array()
                                                                )
                                            )
                  )
            ;
        ;
    }
}