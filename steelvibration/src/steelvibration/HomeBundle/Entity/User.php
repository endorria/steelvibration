<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User
{
    /**
     * @var string
     */
    private $loginuser;

    /**
     * @var string
     */
    private $passworduser;

    /**
     * @var integer
     */
    private $statususer;


    /**
     * Get loginuser
     *
     * @return string 
     */
    public function getLoginuser()
    {
        return $this->loginuser;
    }

    /**
     * Set passworduser
     *
     * @param string $passworduser
     * @return User
     */
    public function setPassworduser($passworduser)
    {
        $this->passworduser = $passworduser;
    
        return $this;
    }

    /**
     * Get passworduser
     *
     * @return string 
     */
    public function getPassworduser()
    {
        return $this->passworduser;
    }

    /**
     * Set statususer
     *
     * @param integer $statususer
     * @return User
     */
    public function setStatususer($statususer)
    {
        $this->statususer = $statususer;
    
        return $this;
    }

    /**
     * Get statususer
     *
     * @return integer 
     */
    public function getStatususer()
    {
        return $this->statususer;
    }
}
