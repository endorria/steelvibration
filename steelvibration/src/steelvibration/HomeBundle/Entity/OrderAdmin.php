<?php
namespace steelvibration\HomeBundle\Entity;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class OrderAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('billingAddress', 'text',  array('label' => 'Billing Address'))
            ->add('deliveryAddress', 'text', array('label' => 'Delivery Address'))
            ->add('nameCustomer','text',     array('label' => 'Customer Last Name' ))
            ->add('prenomCustomer','text',   array('label' => 'Customer First Name'))
            ->add('phoneCustomer','text',    array('label' => 'Customer Phone' ))
            ->add('emailCustomer','text',    array('label' => 'Customer Email' ))
            ->add('idordercustomerinformation' ,'sonata_type_collection', array('label' => 'Album',
                                                                          // 'target_entity' => '\steelvibration\HomeBundle\Entity\Category',
                                                                           //'cascade_validation' => true,
                                                                           'by_reference' => false,
                                                                           ),
                                                                     array(
                                                                           'edit' => 'inline',
                                                                           'inline' => 'table'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('billingAddress')
            ->add('deliveryAddress')
            ->add('nameCustomer')
            ->add('prenomCustomer')
            ->add('phoneCustomer')
            ->add('emailCustomer')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('slug')
            ->add('author')
        ;
    }
}