<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Album
 */
class Album
{
    /**
     * @var integer
     */
    private $idalbum;

    /**
     * @var string
     */
    private $titlealbum;

    /**
     * @var float
     */
    private $pricealbum;

    /**
     * @var string
     *
     * @ORM\Column(name="imagePath", type="string", length=60, nullable=true)
     */
    private $imagepath;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $idordercustomerinformation;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idordercustomerinformation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get idalbum
     *
     * @return integer 
     */
    public function getIdalbum()
    {
        return $this->idalbum;
    }

    /**
     * Set titlealbum
     *
     * @param string $titlealbum
     * @return Album
     */
    public function setTitlealbum($titlealbum)
    {
        $this->titlealbum = $titlealbum;
    
        return $this;
    }

    /**
     * Get titlealbum
     *
     * @return string 
     */
    public function getTitlealbum()
    {
        return $this->titlealbum;
    }

    /**
     * Set pricealbum
     *
     * @param float $pricealbum
     * @return Album
     */
    public function setPricealbum($pricealbum)
    {
        $this->pricealbum = $pricealbum;
    
        return $this;
    }

    /**
     * Get pricealbum
     *
     * @return float 
     */
    public function getPricealbum()
    {
        return $this->pricealbum;
    }

    /**
     * Set imagepath
     *
     * @param string $imagepath
     * @return Album
     */
    public function setImagepath($imagepath)
    {
        $this->imagepath = $imagepath;
    
        return $this;
    }

    /**
     * Get imagepath
     *
     * @return string 
     */
    public function getImagepath()
    {
        return $this->imagepath;
    }

    /**
     * Add idordercustomerinformation
     *
     * @param \steelvibration\HomeBundle\Entity\Customerinformation $idordercustomerinformation
     * @return Album
     */
    public function addIdordercustomerinformation(\steelvibration\HomeBundle\Entity\Customerinformation $idordercustomerinformation)
    {
        $this->idordercustomerinformation[] = $idordercustomerinformation;
    
        return $this;
    }

    /**
     * Remove idordercustomerinformation
     *
     * @param \steelvibration\HomeBundle\Entity\Customerinformation $idordercustomerinformation
     */
    public function removeIdordercustomerinformation(\steelvibration\HomeBundle\Entity\Customerinformation $idordercustomerinformation)
    {
        $this->idordercustomerinformation->removeElement($idordercustomerinformation);
    }

    /**
     * Get idordercustomerinformation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdordercustomerinformation()
    {
        return $this->idordercustomerinformation;
    }
}
