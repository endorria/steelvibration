<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Media
 */
class Media
{
    /**
     * @var integer
     */
    private $idmedia;

    /**
     * @var string
     */
    private $titlemedia;

    /**
     * @var string
     */
    private $descriptionmedia;

    /**
     * @var string
     */
    private $pathmedia;

    /**
     * @var integer
     */
    private $typemedia;

    /**
     * @var \steelvibration\HomeBundle\Entity\Mediaalbum
     */
    private $mediaalbummediaalbum;


    /**
     * Get idmedia
     *
     * @return integer 
     */
    public function getIdmedia()
    {
        return $this->idmedia;
    }

    /**
     * Set titlemedia
     *
     * @param string $titlemedia
     * @return Media
     */
    public function setTitlemedia($titlemedia)
    {
        $this->titlemedia = $titlemedia;
    
        return $this;
    }

    /**
     * Get titlemedia
     *
     * @return string 
     */
    public function getTitlemedia()
    {
        return $this->titlemedia;
    }

    /**
     * Set descriptionmedia
     *
     * @param string $descriptionmedia
     * @return Media
     */
    public function setDescriptionmedia($descriptionmedia)
    {
        $this->descriptionmedia = $descriptionmedia;
    
        return $this;
    }

    /**
     * Get descriptionmedia
     *
     * @return string 
     */
    public function getDescriptionmedia()
    {
        return $this->descriptionmedia;
    }

    /**
     * Set pathmedia
     *
     * @param string $pathmedia
     * @return Media
     */
    public function setPathmedia($pathmedia)
    {
        $this->pathmedia = $pathmedia;
    
        return $this;
    }

    /**
     * Get pathmedia
     *
     * @return string 
     */
    public function getPathmedia()
    {
        return $this->pathmedia;
    }

    /**
     * Set typemedia
     *
     * @param integer $typemedia
     * @return Media
     */
    public function setTypemedia($typemedia)
    {
        $this->typemedia = $typemedia;
    
        return $this;
    }

    /**
     * Get typemedia
     *
     * @return integer 
     */
    public function getTypemedia()
    {
        return $this->typemedia;
    }

    /**
     * Set mediaalbummediaalbum
     *
     * @param \steelvibration\HomeBundle\Entity\Mediaalbum $mediaalbummediaalbum
     * @return Media
     */
    public function setMediaalbummediaalbum(\steelvibration\HomeBundle\Entity\Mediaalbum $mediaalbummediaalbum = null)
    {
        $this->mediaalbummediaalbum = $mediaalbummediaalbum;
    
        return $this;
    }

    /**
     * Get mediaalbummediaalbum
     *
     * @return \steelvibration\HomeBundle\Entity\Mediaalbum 
     */
    public function getMediaalbummediaalbum()
    {
        return $this->mediaalbummediaalbum;
    }

}
