<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idArticle", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idarticle;

    /**
     * @var string
     *
     * @ORM\Column(name="titleArticle", type="text", nullable=false)
     */
    private $titlearticle;

    /**
     * @var string
     *
     * @ORM\Column(name="contentArticle", type="text", nullable=true)
     */
    private $contentarticle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="depositeDateArticle", type="datetime", nullable=true)
     */
    private $depositedatearticle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="targetedDateArticle", type="datetime", nullable=true)
     */
    private $targeteddatearticle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDateArticle", type="datetime", nullable=true)
     */
    private $modifieddatearticle;

    /**
     * @var string
     *
     * @ORM\Column(name="authorArticle", type="string", length=45, nullable=true)
     */
    private $authorarticle;

    /**
     * @var string
     *
     * @ORM\Column(name="pathImage", type="string", length=100, nullable=true)
     */
    private $pathimage;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=45, nullable=true)
     */
    private $category;



    /**
     * Get idarticle
     *
     * @return integer 
     */
    public function getIdarticle()
    {
        return $this->idarticle;
    }

    /**
     * Set titlearticle
     *
     * @param string $titlearticle
     * @return Article
     */
    public function setTitlearticle($titlearticle)
    {
        $this->titlearticle = $titlearticle;
    
        return $this;
    }

    /**
     * Get titlearticle
     *
     * @return string 
     */
    public function getTitlearticle()
    {
        return $this->titlearticle;
    }

    /**
     * Set contentarticle
     *
     * @param string $contentarticle
     * @return Article
     */
    public function setContentarticle($contentarticle)
    {
        $this->contentarticle = $contentarticle;
    
        return $this;
    }

    /**
     * Get contentarticle
     *
     * @return string 
     */
    public function getContentarticle()
    {
        return $this->contentarticle;
    }

    /**
     * Set depositedatearticle
     *
     * @param \DateTime $depositedatearticle
     * @return Article
     */
    public function setDepositedatearticle($depositedatearticle)
    {
        $this->depositedatearticle = $depositedatearticle;
    
        return $this;
    }

    /**
     * Get depositedatearticle
     *
     * @return \DateTime 
     */
    public function getDepositedatearticle()
    {
        return $this->depositedatearticle;
    }

    /**
     * Set targeteddatearticle
     *
     * @param \DateTime $targeteddatearticle
     * @return Article
     */
    public function setTargeteddatearticle($targeteddatearticle)
    {
        $this->targeteddatearticle = $targeteddatearticle;
    
        return $this;
    }

    /**
     * Get targeteddatearticle
     *
     * @return \DateTime 
     */
    public function getTargeteddatearticle()
    {
        return $this->targeteddatearticle;
    }

    /**
     * Set modifieddatearticle
     *
     * @param \DateTime $modifieddatearticle
     * @return Article
     */
    public function setModifieddatearticle($modifieddatearticle)
    {
        $this->modifieddatearticle = $modifieddatearticle;
    
        return $this;
    }

    /**
     * Get modifieddatearticle
     *
     * @return \DateTime 
     */
    public function getModifieddatearticle()
    {
        return $this->modifieddatearticle;
    }

    /**
     * Set authorarticle
     *
     * @param string $authorarticle
     * @return Article
     */
    public function setAuthorarticle($authorarticle)
    {
        $this->authorarticle = $authorarticle;
    
        return $this;
    }

    /**
     * Get authorarticle
     *
     * @return string 
     */
    public function getAuthorarticle()
    {
        return $this->authorarticle;
    }

    /**
     * Set pathimage
     *
     * @param string $pathimage
     * @return Article
     */
    public function setPathimage($pathimage)
    {
        $this->pathimage = $pathimage;
    
        return $this;
    }

    /**
     * Get pathimage
     *
     * @return string 
     */
    public function getPathimage()
    {
        return $this->pathimage;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Article
     */
    public function setCategory($category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
