<?php

namespace steelvibration\HomeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mediaalbum
 */
class Mediaalbum
{
    /**
     * @var integer
     */
    private $idmediaalbum;

    /**
     * @var string
     */
    private $titremediaalbum;

    /**
     * @var string
     */
    private $typemediaalbum;


    /**
     * Get idmediaalbum
     *
     * @return integer 
     */
    public function getIdmediaalbum()
    {
        return $this->idmediaalbum;
    }

    /**
     * Set titremediaalbum
     *
     * @param string $titremediaalbum
     * @return Mediaalbum
     */
    public function setTitremediaalbum($titremediaalbum)
    {
        $this->titremediaalbum = $titremediaalbum;
    
        return $this;
    }

    /**
     * Get titremediaalbum
     *
     * @return string 
     */
    public function getTitremediaalbum()
    {
        return $this->titremediaalbum;
    }

    /**
     * Set typemediaalbum
     *
     * @param string $typemediaalbum
     * @return Mediaalbum
     */
    public function setTypemediaalbum($typemediaalbum)
    {
        $this->typemediaalbum = $typemediaalbum;
    
        return $this;
    }

    /**
     * Get typemediaalbum
     *
     * @return string 
     */
    public function getTypemediaalbum()
    {
        return $this->typemediaalbum;
    }
}
