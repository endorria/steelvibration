<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';
$loader->add('Sonata', __DIR__);

/*'Sonata' => __DIR__.'/../vendor/bundles',
    'Exporter' => __DIR__.'/../vendor/exporter/lib',
    'Knp\Bundle' => __DIR__.'/../vendor/bundles',
    'Knp\Menu' => __DIR__.'/../vendor/KnpMenu/src',*/
// app/autoload.php

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));


return $loader;
