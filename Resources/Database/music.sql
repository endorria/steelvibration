-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 24 Juin 2014 à 10:36
-- Version du serveur :  5.6.16
-- Version de PHP :  5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

INSERT INTO `album` (`titleAlbum`, `priceAlbum`) VALUES
("Zoomba", 10),
("Yolo", 20);

INSERT INTO `music` (`idMusic`, `titleMusic`, `pathMusic`, `Album_idAlbum`, `isInPlaylist`) VALUES
(1, 'a', 'audio/a.mp3', 1, 1),
(2, 'b', 'audio/b.mp3', 1, 1),
(3, 'c', 'audio/c.mp3', 1, 0),
(4, '02. When a man is poor.mp3', 'audio/KITCHENER the grand master/02. When a man is poor.mp3', 1, 1);


