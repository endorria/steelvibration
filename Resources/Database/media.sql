-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 27 Juin 2014 à 04:09
-- Version du serveur :  5.6.16
-- Version de PHP :  5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `steelvibrationdb_0.0.4-marc`
--

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `idMedia` int(11) NOT NULL AUTO_INCREMENT,
  `titleMedia` varchar(45) NOT NULL,
  `descriptionMedia` varchar(100) DEFAULT NULL,
  `pathMedia` varchar(200) NOT NULL,
  `typeMedia` int(11) NOT NULL,
  `mediaalbum_idmediaalbum` int(11) NOT NULL,
  PRIMARY KEY (`idMedia`),
  KEY `fk_Media_mediaalbum1_idx` (`mediaalbum_idmediaalbum`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `media`
--

INSERT INTO `media` (`idMedia`, `titleMedia`, `descriptionMedia`, `pathMedia`, `typeMedia`, `mediaalbum_idmediaalbum`) VALUES
(1, 'accordage avec Laurent Lalsingué', NULL, 'albums/photos/accordage/accordage1.jpg', 0, 1),
(2, 'accordage avec Laurent Lalsingué2', NULL, 'albums/photos/accordage/accordage avec Laurent Lalsingué2.jpg', 0, 1),
(3, 'accordage avec Laurent Lalsingué3', NULL, 'albums/photos/accordage/accordage avec Laurent Lalsingué3.jpg', 0, 1),
(4, 'accordage avec Laurent Lalsingué4', NULL, 'albums/photos/accordage/accordage avec Laurent Lalsingué4.jpg', 0, 1),
(5, 'accordage avec Laurent Lalsingué5', NULL, 'albums/photos/accordage/accordage avec Laurent Lalsingué5.jpg', 0, 1),
(6, 'accordage avec Laurent Lalsingué6', NULL, 'albums/photos/accordage/accordage avec Laurent Lalsingué6.jpg', 0, 1),
(7, 'Stage steelband enfants', NULL, 'https://www.youtube.com/watch?v=DRcyhVHaDfE', 1, 2),
(8, 'Les enfants ça groove !', NULL, 'https://www.youtube.com/watch?v=O6gArizZy9s', 1, 2),
(9, 'Atelier steelband concert" Matilda"', NULL, 'https://www.youtube.com/watch?v=jgWLkGITjxE', 1, 2),
(10, 'Atelier Steelband concert" Le Lion est mort c', NULL, 'https://www.youtube.com/watch?v=RKGe_EhHsIQ ', 1, 2);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `fk_Media_mediaalbum1` FOREIGN KEY (`mediaalbum_idmediaalbum`) REFERENCES `mediaalbum` (`idmediaalbum`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
