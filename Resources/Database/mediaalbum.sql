-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 27 Juin 2014 à 04:09
-- Version du serveur :  5.6.16
-- Version de PHP :  5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `steelvibrationdb_0.0.4-marc`
--

-- --------------------------------------------------------

--
-- Structure de la table `mediaalbum`
--

CREATE TABLE IF NOT EXISTS `mediaalbum` (
  `idmediaalbum` int(11) NOT NULL AUTO_INCREMENT,
  `titreMediaAlbum` varchar(200) NOT NULL,
  `typeMediaAlbum` varchar(45) NOT NULL,
  PRIMARY KEY (`idmediaalbum`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `mediaalbum`
--

INSERT INTO `mediaalbum` (`idmediaalbum`, `titreMediaAlbum`, `typeMediaAlbum`) VALUES
(1, 'Accordage', 'photo'),
(2, 'Atelier SteelBand', 'video');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
