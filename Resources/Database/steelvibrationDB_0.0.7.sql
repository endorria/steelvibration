-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 26 Juin 2014 à 10:10
-- Version du serveur :  5.6.16
-- Version de PHP :  5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- -----------------------------------------------------
-- Schema steelvibrationdb_0.0.7
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `steelvibrationdb_0.0.7` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `steelvibrationdb_0.0.7` ;

-- --------------------------------------------------------

--
-- Structure de la table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `idAlbum` int(11) NOT NULL AUTO_INCREMENT,
  `titleAlbum` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priceAlbum` double DEFAULT NULL,
  `imagePath` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idAlbum`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `album`
--

INSERT INTO `album` (`idAlbum`, `titleAlbum`, `priceAlbum`, `imagePath`) VALUES
(1, 'playlist', 50, 'album_playlist.jpg'),
(2, 'Premier album', 50, 'album_premierAlbum.jpg'),
(3, 'Deuxieme album', 100, 'album_deuxiemeAlbum.jpg'),
(4, 'Troisieme album', 100, 'album_troisiemeAlbum.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `idArticle` int(11) NOT NULL AUTO_INCREMENT,
  `titleArticle` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contentArticle` longtext COLLATE utf8_unicode_ci,
  `depositeDateArticle` datetime DEFAULT NULL,
  `targetedDateArticle` datetime DEFAULT NULL,
  `modifiedDateArticle` datetime DEFAULT NULL,
  `authorArticle` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pathImage` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Category_idCategory` int(11) DEFAULT NULL,
  `User_idBaseUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idArticle`),
  KEY `IDX_23A0E66557515F0` (`Category_idCategory`),
  KEY `IDX_23A0E661DA01C3D` (`User_idBaseUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`idArticle`, `titleArticle`, `contentArticle`, `depositeDateArticle`, `targetedDateArticle`, `modifiedDateArticle`, `authorArticle`, `pathImage`, `Category_idCategory`, `User_idBaseUser`) VALUES
(1, 'test', 'cinhezgrthyjukflhmj', '2014-06-04 00:00:00', '2014-06-04 00:00:00', '2014-06-06 00:00:00', 'Guilllaum', '/Zf/Erg/EG', 1, 5),
(2, 'article2', 'bibabeloula bibabeloula bibabeloula bibabeloula bibabeloula ', '2014-06-20 00:00:00', '2014-06-04 00:00:00', '2014-06-06 00:00:00', 'Guilllaume', '/Zf/Erg/EG', 1, 5),
(3, 'article3', 'tchiqui tchiqui tchiqui tchiqui tchiqui tchiqui tchiqui ', '2014-06-20 00:00:00', '2014-06-04 00:00:00', '2014-06-06 00:00:00', 'Guilllaume', '/Zf/Erg/EG', 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `basegroup`
--

CREATE TABLE IF NOT EXISTS `basegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A686FA2E5E237E06` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `basegroup`
--

INSERT INTO `basegroup` (`id`, `name`, `roles`) VALUES
(1, 'admin', 'ROLE_ADMIN');

-- --------------------------------------------------------

--
-- Structure de la table `baseuser`
--

CREATE TABLE IF NOT EXISTS `baseuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AA9EB1F992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_AA9EB1F9A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `baseuser`
--

INSERT INTO `baseuser` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(2, 'absy', 'absy', 'absy@test.com', 'absy@test.com', 1, 'lbg9esz41f4o4s4kwkkowgoc4808skk', 'JVL8LDZgdlcOTMm7PIyvhuFVgPZGi0CeuEyBxc311p+Gj6bCZMKHjHnD8oldJXmMJtQNDPIyKHCEbLUgwZtZjQ==', '2014-06-25 12:59:51', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-06-24 21:53:27', '2014-06-25 12:59:51', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(5, 'admin', 'admin', 'admin@sv.fr', 'admin@sv.fr', 1, 'tonpmwx3dggks0g8cgo08w44ks0koks', 'KVQLJp+27+6DureXli6yU07T9BpndQmIqIm3rG7VqQbsmGurvuNgVMFxIgW0uv7av3c39nLuGXwWopuYKjbUIg==', '2014-06-25 23:26:53', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-06-25 16:29:35', '2014-06-25 23:26:53', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `idCategory` int(11) NOT NULL AUTO_INCREMENT,
  `nameCategory` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `descriptionCategory` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`idCategory`, `nameCategory`, `descriptionCategory`) VALUES
(1, 'Event', 'Evenements');
INSERT INTO `category` (`idCategory`, `nameCategory`, `descriptionCategory`) VALUES
(2, 'News', 'Actualites');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `idComment` int(11) NOT NULL,
  `contentComment` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorComment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depositeDateComment` datetime DEFAULT NULL,
  `Article_idArticle` int(11) NOT NULL,
  PRIMARY KEY (`idComment`,`Article_idArticle`),
  KEY `IDX_9474526CE615074A` (`Article_idArticle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `customerinformation`
--

CREATE TABLE IF NOT EXISTS `customerinformation` (
  `idCustomerInformation` int(11) NOT NULL AUTO_INCREMENT,
  `billingAddress` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliveryAddress` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nameCustomer` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenomCustomer` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phoneCustomer` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailCustomer` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idCustomerInformation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `customerinformation`
--

INSERT INTO `customerinformation` (`idCustomerInformation`, `billingAddress`, `deliveryAddress`, `nameCustomer`, `prenomCustomer`, `phoneCustomer`, `emailCustomer`) VALUES
(1, '2 rue europe 33459,r', '4 rue ALeroiib', 'SY', 'Walid', '09-23-234546', 'walid@test.com');

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_user_group`
--

CREATE TABLE IF NOT EXISTS `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_B3C77447A76ED395` (`user_id`),
  KEY `IDX_B3C77447FE54D947` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `idMedia` int(11) NOT NULL AUTO_INCREMENT,
  `titleMedia` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descriptionMedia` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pathMedia` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeMedia` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMedia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `music`
--

CREATE TABLE IF NOT EXISTS `music` (
  `idMusic` int(11) NOT NULL AUTO_INCREMENT,
  `titleMusic` varchar(100) DEFAULT NULL,
  `pathMusic` varchar(200) DEFAULT NULL,
  `Album_idAlbum` int(11) NOT NULL,
  `isInPlaylist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`idMusic`),
  KEY `fk_Music_Album1_idx` (`Album_idAlbum`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `music`
--

INSERT INTO `music` (`idMusic`, `titleMusic`, `pathMusic`, `Album_idAlbum`, `isInPlaylist`) VALUES
(1, 'a', 'audio/a.mp3', 1, 1),
(2, 'b', 'audio/b.mp3', 1, 1),
(3, 'c', 'audio/c.mp3', 1, 0),
(4, '02. When a man is poor.mp3', 'audio/KITCHENER the grand master/02. When a man is poor.mp3', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `idOrderAlbum` int(11) NOT NULL,
  `idOrderCustomerInformation` int(11) NOT NULL,
  PRIMARY KEY (`idOrderAlbum`,`idOrderCustomerInformation`),
  KEY `IDX_F52993986A7915A1` (`idOrderAlbum`),
  KEY `IDX_F5299398B0F6F04D` (`idOrderCustomerInformation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E661DA01C3D` FOREIGN KEY (`User_idBaseUser`) REFERENCES `baseuser` (`id`),
  ADD CONSTRAINT `FK_23A0E66557515F0` FOREIGN KEY (`Category_idCategory`) REFERENCES `category` (`idCategory`);

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526CE615074A` FOREIGN KEY (`Article_idArticle`) REFERENCES `article` (`idArticle`);

--
-- Contraintes pour la table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `baseuser` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `basegroup` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `music`
--
ALTER TABLE `music`
  ADD CONSTRAINT `fk_Music_Album1` FOREIGN KEY (`Album_idAlbum`) REFERENCES `album` (`idAlbum`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_F52993986A7915A1` FOREIGN KEY (`idOrderAlbum`) REFERENCES `album` (`idAlbum`),
  ADD CONSTRAINT `FK_F5299398B0F6F04D` FOREIGN KEY (`idOrderCustomerInformation`) REFERENCES `customerinformation` (`idCustomerInformation`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
